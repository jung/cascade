# CASCADE

# Welcome

CASCADE is a Monte Carlo event generator based on Transverse Momentum Dependent (TMD) parton densities. 
Hard processes which are generated in collinear factorization with LO multileg or NLO parton level generators are extended by adding transverse momenta to the initial partons according to TMD densities and applying dedicated TMD parton showers and hadronization.

Processes with off-shell kinematics within \kt-factorization, either internally implemented or from external packages via LHE files, can be processed for parton showering and hadronization.

The initial state parton shower is tied to the TMD parton distribution, with all parameters fixed by the TMD distribution.

Get in touch via the developer mailing list if you need any assistance: [cascade-authors@desy.de](mailto:cascade-authors@desy.de)

## Getting started

### Installation with bootstrap

1. create installation directory:

   mkdir MCgenerator_install
   
   cd MCgenerator_install
   
   mkdir install local
   
2. do installation

   chmod a+x CASCADE-bootstrap
   
   ./CASCADE-bootstrap
   
3. test

   cd  MCgenerator_install/local/share/cascade/LHE
   
   ../../../bin/cascade < steering-DY-MCatNLO.txt  


[Installation step-by-step](doc/Installation.md)



