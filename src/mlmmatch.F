*--   AUTHOR :    MICHELANGELO MANGANO
C----------------------------------------------------------------------
      SUBROUTINE UPVETO_MLM(P,PPART,RCLUS,ETCLUS,ETACLMAX,NLJETS,NFJPART
     $     ,IEXC,IPVETO)
C----------------------------------------------------------------------
C     SUBROUTINE TO IMPLEMENT THE MLM JET MATCHING CRITERION
C     USING FASTJET TO FORM CLUSTERS WITH ANTI-KT
C----------------------------------------------------------------------
      IMPLICIT NONE
      INTEGER ITMP
      INTEGER IPVETO,IVETO
C     JET VARIABLES
      INTEGER NLJETS,NJMAX,NCLUS,NCJET
      DOUBLE PRECISION PI
      PARAMETER (PI=3.141593D0)
      PARAMETER (NJMAX=1000)
      DOUBLE PRECISION P(4,20),PCLUS(4,NJMAX),PCJET(4,NJMAX)
     $     ,ETJET(NJMAX),ETAJ(NJMAX),PHIJ(NJMAX),DRJ(NJMAX,NJMAX)
C     PARTONIC VARIABLES FOR FASTJET
      INTEGER NFJPART,NMAX,IEXC
      PARAMETER (NMAX=NJMAX)
      DOUBLE PRECISION PPART(4,NMAX),RCLUS,ETCLUS,ETACLMAX
C
      DOUBLE PRECISION PSERAP
      INTEGER K(NJMAX),KP(NJMAX),KPJ(NJMAX)
C LOCAL VARIABLES
      INTEGER I,J,IHEP,NMATCH,JRMIN
      DOUBLE PRECISION ETAJET,PHIJET,DELR,DPHI,DELRMIN
      DOUBLE PRECISION PT(20),ETA(20),PHI(20)
C
      DOUBLE PRECISION ETMIN  !,ETMAX
      DOUBLE PRECISION TINY
      PARAMETER (TINY=1D-3)
      INTEGER IJMAP(NJMAX)
C
C     INPUTS:
C     - NLJETS: number of hard partons in the matrix element
C     - NFJPART: number of partons in the shower
C     - IEXC:  if 0, then exlcusive jet multiplcity matching, of 1
C     inclusive (accepts njets>npartons)
C     - P(I,J): I=1,4 labels px,py,pz,E, J=1,...,NLJETS labels the final
C     state partons
C     from the matrix element
C     - PPART(I,J): I=1,4 labels px,py,pz,E J=1,...,NFJPART labels the
C     partons from the shower evolution, including those emitted form
C     the initial state
C     - RCLUS: the jet clustering radius (to be set equal to the deltaR
C     separation between hard partosns, used in the ME generation)
C     - ETCLUS: MINIMUM ET OF THE RECONSTRUCTED JETS (to be set slightly
C     larger the the generation pt cut on the matrix element partons)
C     - ETACLMAX: MAX ETA OF THE RECONSTRUCTED JETS (to be set equal to
C     the maximum eta of the generated partons)
C     OUTPUTS:
C     - NCLUS: number of fastjet jets
C      PCLUS(I,J): I=1,4 labels E,ps,py,pz, J=1,...,NCLUS labels the
C     jets recostructed from fastjet
C     - IPVETO: if 0, the event passed the matching, if 1 the event is vetoed
C
      IPVETO=0
      IF(NLJETS.EQ.0.AND.IEXC.EQ.0) RETURN
C
C     PARTON-LEVEL EVENT KINEMATIC
C
      DO I=1,NLJETS
        PT(I)=SQRT(P(1,I)**2+P(2,I)**2)
        ETA(I)=-LOG(TAN(0.5D0*ATAN2(PT(I)+TINY,P(3,I))))
        PHI(I)=ATAN2(P(2,I),P(1,I))
c        write(6,*) ' parton level      ', (P(j,I),J=1,4)
      ENDDO

      IF(NLJETS.GT.0) CALL ALPSOR(PT,NLJETS,KP,2)
C     RECONSTRUCT SHOWERED JETS:
C     FASTJET CLUSTERING, RETURNS FASTJET JETS
      CALL FJCLUS(RCLUS,-1d0,PPART,NFJPART,PCLUS,NCLUS)
      do i=1,NFJPART
c      write(6,*) ' parton shower level ', (Ppart(j,I),J=1,4)
      enddo
C     APPLY PT/ETA CUTS ON JETS
      CALL FJSORT(ETCLUS,ETACLMAX,PCLUS,NCLUS,PCJET,ETJET
     $     ,ETAJ,PHIJ,DRJ ,NCJET, IJMAP )
C     NCJET IS THE NUMBER OF JETS THAT PASS THE ET_MIN AND ETA_MAX CUTS
C     PCJET ARE THE MOMENTA OF THE JETS THAT PASS THE CUTS, ETJET, ETAJ, DHIJ
C     ARE THE ET, ETA, PHI OF THESE JETS, DRJ(I,J) ARE THE DELTA_R
C     SEPARATIONS BETWEEN THESE JETS
c
C     ANALYSE ONLY EVENTS WITH AT LEAST NLJETS-RECONSTRUCTED JETS
c      write(6,*) ' mlmmatch NCJET,NLJETS: ',NCJET,NLJETS
      IF(NCJET.LT.NLJETS) THEN
        GOTO 999
      ENDIF
C
      IF(NCJET.GT.0) CALL ALPSOR(ETJET,NCJET,K,2)
C     ASSOCIATE PARTONS AND JETS, USING MIN(DELR) AS CRITERION
      NMATCH=0
      DO I=1,NCJET
        KPJ(I)=0
      ENDDO
      ETMIN=1D10
      DO I=1,NLJETS
        DELRMIN=1D5
        DO 110 J=1,NCJET
          IF(KPJ(J).NE.0) GO TO 110
          ETAJET=ETAJ(J)
          PHIJET=PHIJ(J)
          DPHI=ABS(PHI(KP(NLJETS-I+1))-PHIJET)
          IF(DPHI.GT.PI) DPHI=2.*PI-DPHI
          DELR=SQRT((ETA(KP(NLJETS-I+1))-ETAJET)**2+(DPHI)**2)
          IF(DELR.LT.DELRMIN) THEN
            DELRMIN=DELR
            JRMIN=J
          ENDIF
 110    CONTINUE
        IF(DELRMIN.LT.1.5*RCLUS) THEN
          NMATCH=NMATCH+1
          KPJ(JRMIN)=I
          ETMIN=MIN(ETMIN,ETJET(JRMIN))
C     ASSOCIATE PARTONS AND MATCHED JETS:
        ENDIF
      ENDDO
c      write(6,*) ' mlmmatch NMATCH,NLJETS: ',NMATCH,NLJETS
      IF(NMATCH.LT.NLJETS) THEN
        GOTO 999
      ENDIF
C     REJECT EVENTS WITH LARGER JET MULTIPLICITY FROM EXCLUSIVE SAMPLE
c      write(6,*) ' mlmmatch NCjets,NLJETS: ',Ncjet,NLJETS,iexc
      IF(NCJET.GT.NLJETS.AND.IEXC.EQ.1) THEN
        GOTO 999
      ENDIF
C     VETO EVENTS WHERE MATCHED JETS ARE SOFTER THAN NON-MATCHED ONES
      IF(IEXC.NE.1) THEN
        J=NCJET
        DO I=1,NLJETS
C     KPJ(K(J)) IS THE PARTON THAT MATCHES THE J-TH SOFTEST JET. HERE
C     K(NCJET) IS THE HARDEST JET, K(1) IS THE SOFTEST
          IF(KPJ(K(J)).EQ.0) THEN
C     EVENT FAILS SINCE MATCHED JETS ARE NOT THE HARDEST
            GOTO 999
          ENDIF
          J=J-1
        ENDDO
      ENDIF
C
      RETURN
 999  IPVETO=1
c flag the vent as rejected
c      IVETO=IPVETO
c uncomment line below allow the event to arrive to the analysis stage
c      IPVETO=0
      END

      subroutine fjclus(R,palg,p,npart,pjets,njets)
      implicit none
      integer n
      parameter (n=1000)
      integer njets
      double precision R,palg,pjets(4,n)
      double precision p(4,n)
      integer npart
c      R = 0.6d0
c      palg = -1d0 ! 1.0d0 = kt, 0.0d0 = Cam/Aachen, -1.0d0 = anti-kt
      call fjcoreppgenkt(p,npart,R,palg,pjets,njets) ! ... now you have the jet
      end

      subroutine fjsort(ptmin,etamax,pj_in,njets_in,pj_out,ptj
     $     ,etaj,phij,drj,njets_out,ijmap)
c Keeps only jets above ptmin and within etamax.
c Calculates pt, eta, dR
      implicit none
      integer n,njmax
      parameter (n=1000,njmax=n)
      integer njets_in,njets_out,i,j,k,ijet(njmax),ijmap(njmax)
      double precision pj_in(4,n),pj_out(4,njmax),ptj(njmax),etaj(njmax)
     $     ,phij(njmax),drj(njmax,njmax)
      double precision ptmin,etamax,pi,tmp
      parameter (pi=3.14159265)
      do 10 i=1,njets_in
        ijet(i)=0
        ptj(i)=sqrt( pj_in(1,i)**2+pj_in(2,i)**2 )
        if(ptj(i).lt.ptmin) goto 10
        etaj(i)=-log(tan(0.5*atan2(ptj(i),pj_in(3,i))))
        if(abs(etaj(i)).gt.etamax) goto 10
        phij(i)=atan2(pj_in(2,i) , pj_in(1,i))
        ijet(i)=1
 10   continue
      k=0
      njets_out=njets_in
      do i=1,njets_in
        if(ijet(i).eq.1) then
          k=k+1
          ijmap(k)=i
          do j=1,4
            pj_out(j,k)=pj_in(j,i)
          enddo
          ptj(k)=ptj(i)
          etaj(k)=etaj(i)
          phij(k)=phij(i)
        else
          njets_out=njets_out-1
        endif
      enddo
      do i=1,njets_out-1
        do j=i+1,njets_out
          tmp=phij(i)-phij(j)
          if(tmp.ge.pi) then
            tmp=2*pi-tmp
          elseif(tmp.le.-pi) then
            tmp=2*pi+tmp
          endif
          drj(i,j)=sqrt((etaj(i)-etaj(j))**2+tmp**2)
          drj(j,i)=drj(i,j)
        enddo
      enddo
      end

      SUBROUTINE FJCONSTITUENTS(IJET,CONSTITUENT_INDICES
     $     ,NCONSTITUENTS)
      INTEGER    IJET
      INTEGER    CONSTITUENT_INDICES(*)
      INTEGER    nconstituents
      call fjcoreconstituents(ijet,constituent_indices
     $     ,nconstituents)
      end

