//#include <stdio.h>
//#include <stdlib.h>
//#include <string.h>
//#include <math.h>
//#include <cmath>
#include <stdio.h>
#include <string.h>
#include <cstring>
#include <iostream>
#include <zlib.h>

using namespace std;
#define NUM_BUFFERS 8
#define BUFFER_LENGTH 1024

char buf[BUFFER_LENGTH];
gzFile gzfile ;

extern "C" {
    void gzopen_(int & lun, char * mode, char * filname, int &istat ) {
        cout << " *     using gzopen for: " << filname <<  endl;
        int err;
        gzfile = NULL;
        if ((gzfile = gzopen(filname,"r")) == NULL)
        {   cout << " could not open file " << endl;
            //cout << " gzopen error = " << gzerror(gzfile, &err) << endl;
            //cout << " gzopen err = " << err << endl;
            fprintf(stderr, "gzopen err: %s\n", gzerror(gzfile, &err));
            istat = 100;

        }
        else {
            //cout << " gzread: " << gzread(gzfile,ts_buffer,200) << endl;
            //cout << "ts_buffer: " << ts_buffer << endl;
            //cout << " gzread2 : " << gzread(gzfile,ts_buffer,200) << endl;
            //cout << "ts_buffer2 : " << ts_buffer << endl;
        }
    }
    void gzread_(char *tmp_string_out, int &istat ) {

        int len=200;
        int err;
        istat = 0;
        char * test;
        char ts_buffer[200];
        // tmp_string_out = NULL ;
        test = gzgets (gzfile,ts_buffer, len);
        if ( test == NULL ) {
            //cout << " gzread error = " << gzerror(gzfile, &err) << endl;
            //cout << " gzread err = " << err << endl;
            fprintf(stderr, " gzread  err: %s\n", gzerror(gzfile, &err));
            istat = 666;
            strncpy(tmp_string_out, ts_buffer,0);
        }
        else {
            strncpy(tmp_string_out, ts_buffer,200);
            // cout << " gzgets0= " << tmp_string_out << endl;
        }
    }
    void gzclose_(int &istat ) {
        gzclose(gzfile);
    }

}
