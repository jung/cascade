//#include <stdio.h>
//#include <stdlib.h>
//#include <string.h>
//#include <math.h>
//#include <cmath>
#include <stdio.h>
#include <string.h>
#include <cstring>
#include <iostream>
#include "HepMC/PythiaWrapper.h"
#include "HepMC/IO_HEPEVT.h"
#include "HepMC/IO_GenEvent.h"
#include "HepMC/GenEvent.h"
#include "HepMC/IO_AsciiParticles.h"
#include "HepMC/HEPEVT_Wrapper.h"
#include "PythiaHelper.h"
//#include "boost/algorithm/string/trim.hpp"
//#include "HepMC/WeightContainer.h"

using namespace std;

extern "C" {
    extern struct {
        char hepmcout[512];
    } cahepmcout_;
}
#define cahepmcout cahepmcout_
extern "C" {
    extern struct casconvhepmc {
        double w_scale[100], w_pdf[200];
        int nhard, nw_scale, nw_pdf, id_scale[100], id_pdf[200];
    } casconvhepmc_;
}
#define casconvhepmc casconvhepmc_

extern "C" {
    // Instantiate an IO strategy for reading from HEPEVT.
    HepMC::IO_HEPEVT hepevtio;

    int ncount = 0;
    HepMC::GenCrossSection cross;

//void convhepmc_(int & ievent, int & iproc, double & xsec){
    void convhepmc_(int & ievent, int & iproc, double & weight, double & xsec, double & xsece, int & id1pdf, int & id2pdf, double & x1pdf, double & x2pdf, double & QFac, double & pdf1, double & pdf2 ) {

//       string scale[]={"scale1","scale2","scale3","scale4","scale5","scale6","scale7","scale8","scale9","scale10"};
        string scale[]= {"scale_variation1","scale_variation2","scale_variation3","scale_variation4","scale_variation5","scale_variation6","scale_variation7","scale_variation8","scale_variation9","scale_variation10"};
        string tmdpdf[]= {"pdf_variation1","pdf_variation2","pdf_variation3","pdf_variation4","pdf_variation5","pdf_variation6","pdf_variation7","pdf_variation8","pdf_variation9","pdf_variation10",
                          "pdf_variation11","pdf_variation12","pdf_variation13","pdf_variation14","pdf_variation15","pdf_variation16","pdf_variation17","pdf_variation18","pdf_variation19","pdf_variation20",
                          "pdf_variation21","pdf_variation22","pdf_variation23","pdf_variation24","pdf_variation25","pdf_variation26","pdf_variation27","pdf_variation28","pdf_variation29","pdf_variation30",
                          "pdf_variation31","pdf_variation32","pdf_variation33","pdf_variation34","pdf_variation35","pdf_variation36","pdf_variation37","pdf_variation38","pdf_variation39","pdf_variation40"
                         };


        // with the static command, called only once
        static char * outfile = cahepmcout.hepmcout+'\0' ;
        static HepMC::IO_GenEvent ascii_io(outfile,std::ios::out);


//       cout << " convhepmc=" << outfile << "$end"<<endl;
        if ( ncount < 10) {
            if (outfile!=NULL) {
                cout << " convhepmc: filename = " <<  outfile << endl;
            }
            else {
                cout << "  convhepmc: NO filename set " <<endl;
                return ;
            }
            ++ncount;
        }


        // pythia pyhepc routine convert common PYJETS in common HEPEVT
        call_pyhepc( 1 );
        //HepMC::HEPEVT_Wrapper::print_hepevt();
        HepMC::GenEvent* evt = hepevtio.read_next_event();
        //HepMC::IO_HEPEVT::print_inconsistency_errors();

        // HepMC::HEPEVT_Wrapper::check_hepevt_consistency();
        // HepMC::IO_HEPEVT::set_trust_mothers_before_daughters( true );
        //   from version 2.06.09 on:
        //      evt->define_units( HepMC::Units::GEV, HepMC::Units::MM );
        evt->use_units(HepMC::Units::GEV, HepMC::Units::MM);
        evt->set_event_number(ievent);
        evt->set_signal_process_id(iproc);
        evt->weights().push_back(weight);
        // cout <<" convhepmc: weight = " << weight << " Event Nr " << ievent << endl;
        // Convert weight names in MadGraph5 convention to the convention outlined
        // in https://arxiv.org/pdf/1405.1067.pdf, page  162ff.
        if( casconvhepmc.nw_scale > 0) {
            string name ;
            for (int jscale=0; jscale<casconvhepmc.nw_scale; jscale++) {
                int id = casconvhepmc.id_scale[jscale] ;

                if (id==1001) name="MUR1.0_MUF1.0";
                if (id==1002) name="MUR1.0_MUF2.0";
                if (id==1003) name="MUR1.0_MUF0.5";
                if (id==1004) name="MUR2.0_MUF1.0";
                if (id==1005) name="MUR2.0_MUF2.0";
                if (id==1006) name="MUR2.0_MUF0.5";
                if (id==1007) name="MUR0.5_MUF1.0";
                if (id==1008) name="MUR0.5_MUF2.0";
                if (id==1009) name="MUR0.5_MUF0.5";

                evt->weights()[name] = casconvhepmc.w_scale[jscale];
                //cout << " convhepmc: scalenumber " << jscale << " id = " << id << endl;
                // cout << " convhepmc: scale name = " << name << " weight = " << casconvhepmc.w_scale[jscale] << endl;
            }
        }
        if( casconvhepmc.nw_pdf > 0) {
            // Convert PDF weight names to the convention outlined
            // in https://arxiv.org/pdf/1405.1067.pdf, page  162ff.
            // but indicating that it is the TMDlib numbering
            for (int jpdf=0; jpdf<casconvhepmc.nw_pdf; jpdf++) {
                int id = casconvhepmc.id_pdf[jpdf] ;
                string name_pdf = "TMD"+ std::to_string(id);
                evt->weights()[name_pdf] = casconvhepmc.w_pdf[jpdf];
                //cout << " convhepmc: pdfnumber " << jpdf << endl;
                //cout << " convhepmc: pdf name = " << name_pdf << " weight = " << casconvhepmc.w_pdf[jpdf] << endl;
            }
        }
        //evt->weights()["test3"] = 1.9999;

        //      std::cout << " ievent " << ievent << " iproc " << iproc << " xsec " <<xsec<< std::endl;
        // set cross section information set_cross_sectio( xsec, xsec_err)
        const double xsecval = xsec;
        const double xsecerr = xsece ;
        cross.set_cross_section( xsecval, xsecerr );
        evt->set_cross_section( cross );
        // Store PDF information.
        HepMC::PdfInfo pdf( id1pdf, id2pdf, x1pdf, x2pdf, QFac, pdf1, pdf2, 230, 230);
        evt->set_pdf_info(pdf);      // write the event out to the ascii file
        ascii_io << evt;

        delete evt;
    }


}
