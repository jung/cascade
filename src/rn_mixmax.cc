
#include <iostream>
//#include "mixmax.h"
#include <iostream>
#include <iomanip>
#include <random>
#include "mixmax.hpp"										

using namespace std;

bool first;
//rng_state_t S;
//rng_state_t *X = &S;
mixmax_engine gen;

extern "C" { 
    void rn_mixmax_(double & z )
    {
    
    z=gen.get_next_float();
//    cout << " mixmax rn " << z << endl; 
    }
}    

extern "C" { 
    void rn_mixmax_ini_(int & seed )
    {

    gen.seed(seed);  // another way to seed, required by std::random
    cout << " mixmax rn init " << seed<< endl; 
    }
}    