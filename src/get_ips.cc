/*
#include "TH1.h"
#include "TFile.h"
#include "TCanvas.h"
#include <TApplication.h>
#include <TStyle.h>
#include <TROOT.h>
*/
#include <iostream>
using namespace std;

extern "C" {
    int Ncount ;
    void get_ips_(int & ievent, int & ileg, int & nbran, double & x, double & z, double & qprime, double & kt2, int & iflb, int & ifla ) {
        if ( Ncount == 1) {
            cout << " CASCADE: dummy version of get_ips is used "<< endl;
            cout << " Get_ips: ievent = " << ievent << " Ileg = " << ileg << " nbran = " << nbran << endl;
            cout << " Get_ips: q_prime = " << qprime << " x = "<< x << " z = " << z << " kt_2 " << kt2 << endl;
            ++Ncount ;
        }
        // here fill histos
    }
}
