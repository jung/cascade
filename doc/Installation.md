# Installation

1. Get the source from git or the latest release from http://www.desy.de/~jung/cascade
```
  tar xvfz cascade-XXXX.tar.gz
  cd cascade-XXXX
```
Two independent build systems can be used.
 
A. `autools/make`, requires GNU `make`

  2A. Generate the Makefiles (do not use shared libraries)
```
./configure --disable-shared --prefix=Your_Install_Path --with-zlib=Path_To_zlib --with-lhapdf=Path_To_LHAPDF6  --with-tmdlib=Path_To_TMDlib --with-hepmc=Path_To_HEPMC
```
  3A. Compile the binary and Install the executable and PDF files
```
make; make install 
```
B. `cmake` (experimental), requires `cmake` 3.11+ and `make` or `ninja`

  2B. Generate the build files
```
cmake -S your_cascade_source_directory  -B your_compilation_directory -DZLIB_DIR=Path_to_zlib -DPYTHIA8_DIR=Path_to_Pythia8 -DLHAPDF_DIR=Path_to_LHAPDF -DTMDLIB_DIR=Path_to_TMDLIB -DHEPMC2_DIR=Path_to_HepMC2 -DCMAKE_INSTALL_PREFIX=Your_Install_Path

```
  3B. Compile the binary and Install the executable and PDF files
```
cmake --build your_compilation_directory; cmake --install your_compilation_directory 
```

4. The executable is in bin, run it with:
```
export CASEED=1242425
export HEPMCOUT=outfile.hepmc
cd Your_Install_Path/bin
./cascade < $install-path/share/cascade/LHE/steering-DY-MCatNLO.txt
```
