# CASCADE News

# Version 3.2.4 (Oct 2022)

## starting scale for IPS (select in steering):

IPS_Start_Scale = 1 ! =0(default): use LHEscale (=1 use shat) as starting scale for initial stata PS

## Initial state parton shower with TMDs for DIS (select in steering)

IPSForDis = 1 ! switch needed for initial state PS for DIS with LHE file

## select kt_min for TMDs

ktmin = 1 ! kt_min for TMD

## PYTHIA8 fragmentation (select in steering):

pythia6 = 1  ! Hadronisation with PYTHIA6 (=0 PYTHIA8) (default is PYTHIA6)


