* STEERING FILE FOR CASCADE 
*
* number of events to be generated
*
*NEVENT 500000
NEVENT 100
*
*-----------------------------------------------------------------------
* GCAS 0 / ! CASCADE   PARAMETERS
*-----------------------------------------------------------------------
*
*   VAR-NAM  1.INDX  2.INDX NEW-VALUE ! COMMENT IF NEEDED
*   -------  ------  ------  --------  ---------------------------------
*
* +++++++++++++++++ Kinematic parameters +++++++++++++++
*
*
     'PBE1'     1       0        -27.5   ! Beam energy                                 
     'KBE1'     1       0         -11    ! -11: positron, 22: photon  2212: proton 
     'IRE1'     1       0        0       ! 0: beam 1 has no structure 
*     				           ! 1: beam 1 has structure  
*
     'PBE2'     1       0         920.   ! Beam energy  
     'KBE2'     1       0        2212    ! 11: electron, 22: photon 2212: proton
     'IRE2'     1       0        1       ! 0:  beam 3 has no structure 
*                                        ! 1:  beam 2 has structure 
*
*
     'QMIN'     1       0        0.      ! (D=5.0) minimum Q2 to be generated
*     'QMAX'     1       0        100    ! (D=10.D8) maximum Q2 to be generated
*     
     'YMIN'     1       0       0.01     ! (D=0.) YMIN of photon
     'YMAX'     1       0       0.8      ! (D=1.) YMAX of photon
*
*    'THEI'     1       0        0.      ! (D=0.) minimum scattering angle for e
*    'THEM'     1       0        180     ! (D=180.) maximum scattering angle for e
*
     'NFLA'     1       0        4       ! (D=5) nr of flavours used in str.fct
*
     'MULT'     1       0        0       ! (D=0) multiple scattering
*
* +++++++++++++++ Hard subprocess selection ++++++++++++++++++
*
     'IPRO'     1       0         2      ! (D=1) 
*                                        !  2: J/psi g
*
     'IPOL'     1       0         1      ! (D=0)  VM->ll (polariation study)
*
     'IHFL'     1       0         4      ! (D=4) produced flavour for IPRO=11
*                                        ! 4: charm
*                                        ! 5: bottom
*				        
     'PTCU'     1       0        1.      ! (D=0) p_t **2 cut for process
*
*
* ++++++++++++ Parton shower and fragmentation ++++++++++++
*
     'NFRA'     1       0        1       ! (D=1) Fragmentation on=1 off=0
*
     'IFPS'     1       0        3       ! (D=3) Parton shower
*                                        ! 0: off
*				                 ! 1: initial state PS
*                                        ! 2: final state PS
*                                        ! 3: initial and final state PS
     'IFIN'     1       0        1       ! (D=1) scale switch for FPS
*                                        ! 1: 2(m^2_1t+m^2_2t)    
*                                        ! 2: shat     
*                                        ! 3: 2(m^2_1+m^2_2)     
     'SCAF'    1       0         1.      ! (D=1) scale factor for FPS
*
     'ITIM'     1       0        0       ! 0: timlike partons may not shower
*     				           ! 1: timlike partons may shower
*
     'ICCF'     1       0        1       ! (D=1) Evolution equation 
*                                        ! 1: CCFM
*                                        ! 0: DGLAP
*
* +++++++++++++ Structure functions and scales +++++++++++++
*
     'IRAM'     1       0        0       ! (D=0) Running of alpha_em(Q2)
*				                 ! 0: fixed
*				                 ! 1: running
* 
     'IRAS'     1       0        1       ! (D=1) Running of alpha_s(MU2)
*				                 ! 0: fixed alpha_s=0.3 
*				                 ! 1: running
*
     'IQ2S'     1       0        3       ! (D=1) Scale MU2 of alpha_s
*                                        !  1: MU2= 4*m**2 (only for heavy quarks)
*                                        !  2: MU2 = shat(only for heavy quarks)
*                                        !  3: MU2= 4*m**2 + pt**2
*                                        !  4: MU2 = Q2
*                                        !  5: MU2 = Q2 + pt**2
*                                        !  6: MU2 = k_t**2
     'SCAL'     1       0        1.0     !  scale factor for renormalisation scale
     'SCAF'     1       0        1.0     !  scale factor for factorisation scale*
*
*     'IGLU'     1       0        1201   ! (D=1010)Unintegrated gluon density 
*                                        ! > 10000 use TMDlib
*                                        !   (i.e. 101201 for JH-2013-set1)
*                                        !  1201: CCFM set JH-2013-set1 (1201 - 1213)
*                                        !  1301: CCFM set JH-2013-set2 (1301 - 1313)
*                                        !  1001: CCFM J2003 set 1 
*                                        !  1002: CCFM J2003 set 2 
*                                        !  1003: CCFM J2003 set 3 
*                                        !  1010: CCFM set A0
*                                        !  1011: CCFM set A0+
*                                        !  1012: CCFM set A0-
*                                        !  1013: CCFM set A1
*                                        !  1020: CCFM set B0
*                                        !  1021: CCFM set B0+
*                                        !  1022: CCFM set B0-
*                                        !  1023: CCFM set B1
*                                        !  1: CCFM old set JS2001
*                                        !  2: derivative of collinear gluon (GRV)
*       		                       !  3: Bluemlein
*				                 !  4: KMS
*				                 !  5: GBW (saturation model)
*				                 !  6: KMR
*				                 !  7: Ryskin,Shabelski
*  
* ++++++++++++ BASES/SPRING Integration procedure ++++++++++++
*
     'NCAL'     1       0      50000     ! (D=20000) Nr of calls per iteration for bases
*
     'ACC1'     1       0       1.0      ! (D=1) relative prec.(%) for grid optimisation
*
     'ACC2'     1       0       0.5      ! (0.5) relative prec.(%) for integration
*
* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
*
*     'INTE'     1       0        0      ! Interaction type (D=0)
*                                        ! = 0 electromagnetic interaction
*     'KT1 '     1       0      0.44     ! (D=0.0) intrinsic kt for beam 1
*
*     'KT2 '     1       0      0.44     ! (D=0.0) intrinsic kt for beam 2
*
*     'KTRE'     1       0      0.35     ! (D=0.35) primordial kt when non-trivial
*                                        ! target remnant is split into two particles
* Les Houches Accord Interface
     'ILHA'     1       0        0      ! (D=10) Les Houches Accord  
*                                        ! = 0  use internal CASCADE
*                                        ! = 1  write event file
*                                        ! = 10 call PYTHIA for final state PS and remnant frag
*
* path for updf files
     'UPDF'   '../UPDF'
*-----------------------------------------------------------------------
GPYT 0 / ! PYTHIA 6 PARAMETERS
*-----------------------------------------------------------------------
*
*
*   VAR-NAM  1.INDX  2.INDX NEW-VALUE ! COMMENT IF NEEDED
*   -------  ------  ------  --------  ---------------------------------
*
     'PMAS'     4       1        1.6     ! charm mass
     'PMAS'     5       1        4.75    ! bottom mass
     'PMAS'    25       1        125.    ! higgs mass
*
     'PARU'   112      0        0.2      ! lambda QCD
*
* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
*
* alphas parameters
     'MSTU'   111      0         1       ! = 0 : alpha_s is fixed
*                                        !     at the value PARU(111)
*                                        ! =1 ;first-order running alpha_s 
*                                        ! =2 ;second-order running alpha_s 
*
* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
**
*                                       Type of Fragmentation:
*    'MSTJ'     1      0         2       ! (D=1), 1=string, 2=independent
*                                       see also MSTJ(2) and MSTJ(3)
* angular ordering in timelike cascade
     'MSTJ'    48      0         1       ! (D=0), 0=no max. angle,
*                                        !  1=max angle def. in PARJ(85)
     'MSTU'   112      0         4       ! nr of flavours wrt lambda_QCD
     'MSTU'   113      0         3       ! min nr of flavours for alphas
     'MSTU'   114      0         5       ! max nr of flavours for alphas
*
*
* from Professor tune 28/3/2010
     'MSTJ'     11     0         5     !  Bowler
     'PARJ'     47     0       0.8     ! fixed parameter
     'PARJ'     1      0       0.073   ! P(qq)/P(q) (D=0.1)
     'PARJ'     2      0       0.20    ! P(s)/P(u) (D=0.3)
     'PARJ'     3      0       0.94    ! P(us)/P(ud)/P(s)/P(d) (D=0.4)
     'PARJ'     4      0       0.032   ! (1/3)P(ud_1)/P(ud_0) (D=0.050)
     'PARJ'    11      0       0.31    ! P(S=1)d,u (D=0.5)
     'PARJ'    12      0       0.4     ! P(S=1)s   (D=0.6)
     'PARJ'    13      0       0.54    ! P(S=1)c,b (D=0.75)
     'PARJ'    21      0       0.325   ! sigma_q (D=0.360)
     'PARJ'    25      0       0.63    ! Extra eta suppression (D=1.)
     'PARJ'    26      0       0.12    ! Extra eta' suppression (D=0.4)
     'PARJ'    41      0       0.5     ! Lund symm. fragm.: a (D=0.3)
     'PARJ'    42      0       0.6     ! Lund symm. fragm.: b (D=0.58)
     'PARJ'    47      0       0.67    ! rb (D=1.0)
     'PARJ'    81      0       0.29    ! LAMBDA for alpha_s in ps (D=0.29)
     'PARJ'    82      0       1.65    ! Parton shower cut-off (D=1.0)
************************************************************************
************************************************************************
END$
