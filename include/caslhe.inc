      Integer Nf_part
      Double precision normhepmc,weight
      Common/lheinfo/normhepmc,weight,Nf_part

      Integer idP1,idP2
      double Precision xidP1,xidP2,pdf1,pdf2,scale_process,scalup_cas,scalup_kt,pdf1pdf2,kt2idP1,kt2idP2,pdf_weight
      common/lhepdfinfo/idP1,idP2,xidP1,xidP2,pdf1,pdf2,scale_process,scalup_cas,scalup_kt,pdf1pdf2,kt2idP1,kt2idP2,pdf_weight

      character*512 hepmcout,lhefile,Collfile
      Integer Ilhe,iTMDw,Iscale,lheWid,lheNBorn,TMDuncertainty
      Double Precision ScaleFactorMatching
      common/CALHE/ScaleFactorMatching,lhefile,Ilhe,iTMDw,Collfile,Iscale,lheWid,lheNBorn,TMDuncertainty
      Common/CAHEPMCOUT/hepmcout

      Integer nhard,nw_scale,nw_pdf,id_scale(100),id_pdf(200)
      Double Precision w_scale(100),w_pdf(200)
      Common/casconvhepmc/w_scale,w_pdf,nhard,nw_scale,nw_pdf,id_scale,id_pdf
      save /casconvhepmc/
      
      Double Precision Scalfac, kt2min
      Common /lheread/ Scalfac, kt2min
