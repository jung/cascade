C...Inputs for the matching algorithm
      double precision etcjet,rclmax,etaclmax,qcut,clfact
      integer maxjets,minjets,nqmax,iexcfile,ktsche,mektsc,nexcres,excres(30)
      integer nremres, remres(30)
      integer nqmatch,nexcproc,iexcproc(MAXPUP),iexcval(MAXPUP)
      logical nosingrad,showerkt,jetprocs
      common/MEMAIN/etcjet,rclmax,etaclmax,qcut,clfact,maxjets,minjets,nqmax,
     $   iexcfile,ktsche,mektsc,nexcres,excres,nremres,remres,
     $   nqmatch,nexcproc,iexcproc,iexcval,nosingrad,showerkt,jetprocs
